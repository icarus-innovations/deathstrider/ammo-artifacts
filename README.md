### Ammo Artifacts
---
This is an addon for Deathstrider that adds smaller, less valuable artifacts on ammo spawns for people who are always broke or just want something to spawn in ammo spawns.

### Credits and Permissions
---
All the cool people who inspired or helped me make this mod:
- Accensus, coding
- Icarus, sprites (Yes, I'm crediting myself. Yes, I have a massive ego.)

All the assets used in this mod are original or used with permission from the original creator, if you want to use any of the assets in this mod in your own content you must get permission from the orginal creator.